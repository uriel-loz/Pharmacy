<?php include_once('includes/templates/header.php'); ?>

<?php 
	require_once ('includes/funciones/funciones.php');

	$justificacion = validar('motivo');
 ?>

<title>Justificacion</title>
</head>

<body>
	<div class="container-fluid">
		<header>
			<div class="encabezado">
				<h1>Justificacion</h1>
			</div>
		</header>

		<form class="form-horizontal">
			<div class="form-group">
				<label for="" class="col-md-offset-3 col-md-1 control-label">Justificacion:</label>
				<div class="col-md-4">
					<textarea class="form-control" rows="6" ><?php echo $justificacion; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-10">
					<a href="Movimientos.php" class="btn btn-success" role="button">Regresar</a>
				</div>
			</div>
		</form>
	</div>
</body>