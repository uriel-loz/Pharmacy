<?php require_once('includes/funciones/crud_roles.php'); ?>
<?php include_once('includes/templates/header.php'); ?>

<?php  
	require_once('includes/funciones/funciones.php');
	
	$usuario = validar('id');

?>
	<title>Asignación de roles</title>
</head>

<body>
	<body>
	<div class="container">
		<header>
			<div class="encabezado">
				<h1>Roles de los usuarios</h1>
			</div>
		</header>

		<form action="Roles.php" method="post" class="form-horizontal">
		
			<div class="form-group">
				<label class="col-md-1 col-md-offset-3" for="usuario">Usuario:</label>
				<div class="col-md-5">
					<input class="form-control" type="text" name="usuario" value="<?php echo $usuario; ?>">
				</div>
			</div>
			

			<div class="form-group">
				<div class="col-md-offset-5">
					<?php 
						$roles = array(
							'0' => 'Administrador',
							'1' => 'Usuario'
							);

						foreach ($roles as $key => $rol) {

							echo '<label class = "radio-inline">';
							echo "<input type='radio' name = 'rol' value = $key>$rol";
							echo'</label>';

						}
					 ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-1 col-md-offset-3" for="fecha_hora">Fecha y hora:</label>
				<div class="col-md-5">
					<?php $hoy = getdate(); ?>
					<input class="form-control" type="text" name="datos_tiempo" value="<?php echo $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds']; ?>">
				</div>
			</div>

			<div class="col-md-offset-5">
				<button class="btn btn-success btn-lg" type="submit" name="asignar">Asignar</button>
				<a href="AdministrarUsuarios.php" class="btn btn-danger btn-lg" role="button">Cancelar</a>
			</div>
		</form>
	</div>
	
	<?php require_once('includes/templates/footer.php'); ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
