<?php ob_flush(); ?>
<?php include_once('includes/templates/header.php');?>
<?php 
	require_once('includes/funciones/funciones.php');

	try {
		require_once('includes/funciones/bd_conexion.php');
		require_once('includes/funciones/crudmedicamentos.php');

		$obtener = "SELECT * FROM medicamentos, datos_medicamentos;";
		$medicamentos = $conn->query($obtener);

	} catch (Exception $e) {
		$error = $e->getMessage();
	}
 ?>
	
	<title>Administración de los Medicamentos</title>
</head>

<body>
	<header>
		<div class="encabezado">
			<h1>Administración de los Medicamentos</h1>
		</div>
	</header>

	<div class="container-fluid">
		<form action="AdministrarMedicamentos.php" method="post" class="form-horizontal">
			<div class="form-group">
				<label for="medicament" class="col-md-2 col-md-offset-2">Nombre del medicamento:</label>
				<div class="col-md-5">
					<input type="text" name="nombre_medicamento" class="form-control" placeholder="Flanax">
				</div>
			</div>

			<div class="form-group">
				<label for="medicament" class="col-md-2 col-md-offset-2">Ingrediente activo:</label>
				<div class="col-md-5">
					<input type="text" name="ingrediente_activo" class="form-control" placeholder="Naproxeno">
				</div>
			</div>

			<div class="form-group">
				<label for="llegada" class="col-md-2 col-md-offset-3">Numero de lote:</label>
				<div class="col-md-3">
					<input type="text" name="lote" class="form-control" placeholder="EJE125373412">
				</div>
			</div>

			<div class="form-group">
				<label for="llegada" class="col-md-2 col-md-offset-3">Presentación:</label>
				<div class="col-md-3">
					<input type="text" name="presentacion" class="form-control" placeholder="250 ml.">
				</div>
			</div>
								
			<div class="form-group">
				<label for="caducidad" class="col-md-2 col-md-offset-2">Fecha de caducidad: </label>
				<div class="col-md-4">
					<input type="text" name="caducidad" class="form-control" placeholder="2016-10-02">
				</div>
			</div>
				
			<div class="form-group">
				<label for="cantidad" class="col-md-2 col-md-offset-3">Cantidad:</label>
					<div class="col-md-2">
						<input type="text" name="cantidad" class="form-control" placeholder="568">
					</div>
			</div>
				
			<div class="form-group" id="botonesregistro">
				<div class="col-md-offset-5">
					<button type="submit" class="btn btn-primary btn-lg">Registrar</button>
					<a href="PerfilUsuario.php" class="btn btn-default btn-lg" role="button">Cancelar</a>
				</div>
			</div>
		</form>
		
		<hr>

		<div class="table-responsive">
			<table class="table table-striped  table-bordered">
				<thead>
					<tr>
						<th>Nombre del medicamento</th>
						<th>Nombre del ingrediente activo</th>
						<th>Lote</th>
						<th>Presentación</th>
						<th>Cantidad</th>
						<th>Caducidad</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody class="table table-hover" ">
					<?php while( $producto = $medicamentos->fetch_assoc() ): ?>

					 <tr>
						<td><?php echo $producto['nombre']; ?></td>
						<td><?php echo $producto['ingrediente_activo']; ?></td>
						<td><?php echo $producto['numero_lote']; ?></td>
						<td><?php echo $producto['presentacion'] ?></td>
						<td><?php echo $producto['cantidad']; ?></td>
						<td><?php echo $producto['fecha_caducidad']; ?></td>
						<td><a href="EditarMedicamentos.php?id=<?php echo $producto['idmedicamento']; ?>" class="btn btn-warning btn-md" >Modificar</a></td>
					</tr>

					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
	</div>

<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php ob_end_flush(); ?>