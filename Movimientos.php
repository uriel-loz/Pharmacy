<?php include_once('includes/templates/header.php'); ?>
<?php 
	try {

		require_once ('includes/funciones/bd_conexion.php');

		$movimientos = "SELECT * FROM `historial`";
		$historial = $conn->query($movimientos);
		
	} catch (Exception $e) {
		
		$error = $e->getMessage();

	}
 ?>

	<title>Movimientos</title>
</head>

<body>
	<div class="container-fluid">
		<header>
			<div class="encabezado">
				<h1>Movimientos</h1>
			</div>
		</header>
		
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>Usuario</th>
						<th>Tipo de movimiento</th>
						<th>Hora y Fecha</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody">
					<?php while( $obtener = $historial->fetch_assoc() ): ?>
					<tr>
						<td><?php echo $obtener['idusuario']; ?></td>

						<td><?php echo $obtener['movimiento']; ?></td>
			
						<td><?php echo $obtener['fecha_y_hora']; ?></td>
						
						<?php if($obtener['motivo'] == 'Null'): ?>

						<td>
							<a href="#" class="btn btn-success disabled">Consultar Motivo</a>
						</td>
						
						<?php else: ?>	
							<td>
								<a href="ConsultarMotivos.php?motivo=<?php echo $obtener['motivo']; ?>" class="btn btn-success">Consultar Motivo</a>
							</td>
						<?php endif; ?>
					</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>