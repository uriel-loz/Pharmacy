<?php require_once('includes/templates/header.php'); ?>
<?php require_once('includes/funciones/funciones.php'); ?>

<?php 

	try {
		require_once('includes/funciones/bd_conexion.php');

		$sinrol = "SELECT nombre, apellido_paterno, apellido_materno, "; 
		$sinrol .= "usuarios.idusuario, rfc ";
		$sinrol .= "FROM `usuarios` "; 
		$sinrol .= "JOIN `datos_personales` ";
		$sinrol .= "ON usuarios.idusuario = datos_personales.idusuario ";
		$sinrol .= "WHERE NOT EXISTS(SELECT idusuario_asignar FROM `roles` ";
		$sinrol .= "WHERE roles.idusuario_asignar = usuarios.idusuario)";

		$usuario = $conn->query($sinrol);

	} catch (Exception $e) {

		$error = $e->getMessage();

	}
 ?>

	<title>Administración usuarios</title>
</head>

<body>
	<header>
		<div class="encabezado">
			<h1>Administración de los usuarios</h1>
		</div>
	</header>

	<div class="container-fluid">
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>Nombre del Empleado</th>
						<th>Usuario</th>
						<th>R.F.C.</th>
						<th colspan="2">Opciones</th>
					</tr>
				</thead>
				<tbody>

					<?php while ( $obtener = $usuario->fetch_assoc() ):?>

					<tr>
						<td>
							<?php 

								echo $obtener['nombre']." ".$obtener['apellido_paterno']." ".$obtener['apellido_materno'];
							?>	
						</td>
						<td><?php echo $obtener['idusuario']; ?></td>
						<td><?php echo $obtener['rfc']; ?></td>
						<td>
							<a href="Roles.php?id=<?php echo $obtener['idusuario']; ?>" class="btn btn-warning btn-md" role="button">Asignar Puesto</a>
						</td>
						<td><a href="includes/funciones/crud_roles.php?idusuario=<?php echo $obtener['idusuario']; ?>" class="btn btn-danger btn-mds" role="button">Borrar</a></td>
					</tr>

					<?php endwhile; ?>
					
				</tbody>
			</table>
		</div>
	</div>

<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>