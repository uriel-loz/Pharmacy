<?php include_once('includes/templates/header.php'); ?>
<?php 
	try {

		require_once ('includes/funciones/bd_conexion.php');

		$movimientos = "SELECT idusuario, nombre, movimiento, h.cantidad, fecha_y_hora ";
		$movimientos .= "FROM datos_medicamentos AS d ";
		$movimientos .= "JOIN medicamentos AS m ";
		$movimientos .= "ON d.idmedicamento = m.idmedicamento ";
		$movimientos .= "JOIN historial_medicamentos AS h ";
		$movimientos .= "ON d.iddato_medicamento = h.iddato_medicamento;";
		$historial = $conn->query($movimientos);
		
	} catch (Exception $e) {
		
		$error = $e->getMessage();

	}
 ?>

	<title>Movimientos Medicamentos</title>
</head>

<body>
	<div class="container-fluid">
		<header>
			<div class="encabezado">
				<h1>Movimientos Medicamentos</h1>
			</div>
		</header>
		
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>Usuario</th>
						<th>Medicamento</th>
						<th>Tipo de movimiento</th>
						<th>Cantidad</th>
						<th>Hora y Fecha</th>
					</tr>
				</thead>
				<tbody">
					<?php while( $obtener = $historial->fetch_assoc() ): ?>
					<tr>
						<td><?php echo $obtener['idusuario']; ?></td>
						<td><?php echo $obtener['nombre']; ?></td>
						<td><?php echo $obtener['movimiento']; ?></td>
						<td><?php echo $obtener['cantidad']; ?></td>
						<td><?php echo $obtener['fecha_y_hora']; ?></td>
					</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>