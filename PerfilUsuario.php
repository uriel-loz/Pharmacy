<?php ob_start();?>

<?php require_once('includes/templates/header.php');?>

<?php 
	require_once('includes/funciones/funciones.php');
	require_once('includes/funciones/crud_datospersonales.php');
	session_start();
	confirmar_sesion();
	
	$idusuario = $_SESSION['usuario'];

	try {
		
		require_once ('includes/funciones/bd_conexion.php');

		$stmt = $conn->prepare("SELECT u.idusuario, nombre, apellido_paterno, 
								apellido_materno, email, tel, sexo 
								FROM usuarios AS u
								JOIN datos_personales AS d
								ON u.idusuario = d.idusuario
								WHERE u.idusuario = ?;");
		$stmt->bind_param("s", $idusuario);
		$stmt->execute();
		$stmt->bind_result($usuario, $nombre, $apellido_paterno, $apellido_materno,
							 $email, $telefono, $sexo);



	} catch (Exception $e) {

		$error = $e->getMessage();

	}
?>


	
	<title>Perfil Usuario</title>
</head>

<body>
	<header>
		<div class="encabezado">
			<h1>Perfil del usuario</h1>
		</div>
	</header>

	<div class="container main">
        <div class="row">
			<ul class="nav nav-tabs nav-justified" role="tablist">
	  			<li role="presentation" class="active"><a href="#profile"  role="tab" data-toggle="tab">Perfil</a></li>
			  	<li role="presentation"><a href="#information"  role="tab" data-toggle="tab">Actualizar Información</a></li>
			  	<li role="presentation"><a href="#password" role="tab" data-toggle="tab">Cambiar contraseña</a></li>
			</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="profile">
				<form class="form-horizontal">

					<?php while($stmt->fetch()): ?>

					<div class="form-group">
						<label class="col-sm-2 control-label">Usuario:</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?php echo $usuario; ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Nombre:</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?php echo $nombre; ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Apellido paterno:</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?php echo $apellido_paterno; ?></p>
						</div>
					</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Apellido materno:</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?php echo $apellido_materno; ?></p>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Email:</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?php echo $email; ?></p>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Teléfono:</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?php echo $telefono; ?></p>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Sexo:</label>
					<div class="col-sm-10">
						<p class="form-control-static">
							<?php 
								if ($sexo == 'm') {
									echo 'Masculino';
								}else{
									echo 'Femenino';
								}
							 ?>
						</p>
					</div>
				</div>
				
				<?php endwhile; ?>	
				<?php $stmt->close(); ?>
			</form>  <!-- Cambio de opciones del perfil -->
	</div> <!-- fin del main -->

	<div role="tabpanel" class="tab-pane" id="password">
		<form class="form-horizontal" action="PerfilUsuario.php" method="post">
			
			<input type="hidden" name="user" value="<?php echo $idusuario; ?>">

			<div class="form-group">
				<label for="contrasenia" class="col-sm-2 col-sm-offset-2 control-label">Contraseña anterior</label>
				<div class="col-sm-5">
					<input type="password" class="form-control" name="contraseniaactual">
				</div>
			</div>
			<div class="form-group">
				<label for="contrasenia" class="col-sm-2 col-sm-offset-2 control-label">Nueva Contraseña</label>
				<div class="col-sm-5">
					<input type="password" class="form-control" name="nuevacontrasenia">
				</div>
			</div>
			<div class="form-group">
				<label for="contrasenia" class="col-sm-2 col-sm-offset-2 control-label">Repetir Contraseña</label>
				<div class="col-sm-5">
					<input type="password" class="form-control" name="confirmacioncontrasenia">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-5">
					<button type="submit" class="btn btn-primary" name="password">Cambiar</button>
				</div>
			</div>
		</form>
	</div>

<?php try {
		$infousuario = "SELECT nombre, apellido_paterno, apellido_materno, tel, email, sexo ";
		$infousuario .= "FROM `datos_personales` ";
		$infousuario .= "WHERE idusuario = '{$idusuario}' ";

		$datos_personales = $conn->query($infousuario);

		echo $conn->error;

	} catch (Exception $e) {
		$error = $e->getMessage();
	} 
?>

	<div role="tabpanel" class="tab-pane" id="information">
		<form class="form-horizontal" role="form" method="post" action="PerfilUsuario.php">
			
			<?php while($obtener = $datos_personales->fetch_assoc()): ?>

 			<div class="form-group">
   			<label for="nombre" class="col-md-4 control-label">Nombre(s)</label>
    			<div class="col-md-6">
        			<input id="nombre" type="text" class="form-control" name="nombre" value="<?php echo $obtener['nombre']; ?>">
            	</div>
			</div>

			<div class="form-group">
    			<label for="apellido paterno" class="col-md-4 control-label">Apellido Paterno</label>
    			<div class="col-md-6">
        			<input id="apellido paterno" type="text" class="form-control" name="apellido_paterno" value="<?php echo $obtener['apellido_paterno']; ?>">
				</div>
			</div>

			<div class="form-group">
    			<label for="apellido materno" class="col-md-4 control-label">Apellido Materno</label>

    			<div class="col-md-6">
        			<input id="mother_last_name" type="text" class="form-control" name="apellido_materno" value="<?php echo $obtener['apellido_materno']; ?>">
        		</div>
			</div>

			<div class="form-group">
				    <label for="phone" class="col-md-4 control-label">Teléfono</label>
				<div class="col-md-6">
        			<input id="phone" type="text" class="form-control" name="phone" value="<?php echo $obtener['tel']; ?>">
        		</div>
        	</div>

        	<div class="form-group">
				    <label for="phone" class="col-md-4 control-label">Email</label>
				<div class="col-md-6">
        			<input id="phone" type="text" class="form-control" name="email" value="<?php echo $obtener['email']; ?>">
        		</div>
        	</div>


			<div class="form-group">
    			<label for="gender" class="col-md-4 control-label">Sexo</label>
    			<div class="radio">

    				<?php 

    				$genero = array(
    					'm' => 'Masculino',
    					'f' => 'Femenino'
    					);

    				foreach ($genero as $key => $sex) {

    					 echo "<label class='col-md-1' >";
    					
      					 	echo "<input type='radio' name='sexo' value='$key' checked>$sex";
      					
      					 echo "</label>";

    				}
      				
     				?>

     			</div>
			</div>

			<input type="hidden" name="alias" value="<?php echo $idusuario; ?>">

			<div class="form-group">
    			<div class="col-md-6 col-md-offset-4">
        			<button type="submit" class="btn btn-primary" name="actualizar">
            			<i class="fa fa-btn fa-user"></i> Actualizar informacion personal
        			</button>
    			</div>
			</div>

			<?php endwhile; ?>

		</form>								
	</div>
</div>

<?php include_once('includes/templates/footer.php') ?>	

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php ob_end_flush(); ?>