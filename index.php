<?php 
	
	require_once('includes/funciones/funciones.php');

	if (isset($_POST['submit'])) {

		session_start();

		$nombreusuario = validar('nombreusuario');
		$password = validar('contrasenia');
	

		try {
			require_once('includes/funciones/bd_conexion.php');

			$confirmar_inicio = "SELECT u.idusuario, contrasenia, estado ";
			$confirmar_inicio .= "FROM `usuarios` AS u "; 
			$confirmar_inicio .= "JOIN `cuentas` AS c ";
			$confirmar_inicio .= "ON u.idusuario = c.idusuario ";
			$confirmar_inicio .= "WHERE u.idusuario = '{$nombreusuario}'";

			$autorizar = $conn->query($confirmar_inicio);

			echo $conn->error;

			while ($verificar = $autorizar->fetch_assoc()) {

				if (password_verify($password, $verificar['contrasenia'])) {

					$_SESSION['usuario'] = $nombreusuario;

					if ($verificar['estado'] == 1) {
						header('Location: PerfilUsuario.php');
					}else{
						echo "<script>alert('Usuario desabilitado');</script>";
					}
				}else{
					echo "<script>alert('Usuario o Contraseña incorrecta');</script>";
				}
			}

		} catch (Exception $e) {

			$error=$e->getMessage();

		}
	}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="css/Normalize.css">
	<link rel="stylesheet" href="css/theme.min.css">
	<link rel="stylesheet" href="css/estilos.css">
	<title>Farmacia Lazaro Inicio de sesión</title>
</head>
<body>
	<div class="container">
		<div class="encabezado">
			<header>
				<h2 class="h2">Inicio de Sesión</h2>
			</header>
		</div>

		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#boton1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Farmacias Lazaro</a>
				</div>

				<div class="collapse navbar-collapse" id="boton1">
					<ul class="nav navbar-nav">
				        <li class="active"><a href="#">Incio de Sesión<span class="sr-only">(current)</span></a></li>
				        <li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acerca de Nosotros <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="objetivos.html">Objetivos</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="mision.html">Misión</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="vision.html">Visión</a></li>
				          </ul>
				        </li>
				      </ul>
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="DatosPersonales.php">Registrarse</a></li>
				      </ul>
				</div>
			</div><!-- Fin de container-fluid -->
		</nav> <!-- Fin de la barra de navegación -->
		
		<form class="form-horizontal col-md-offset-3" action="index.php" method="post">
		  
		  <div class="form-group">
		    <label for="usuario" class="col-md-2 col-sm-3 control-label">Usuario</label>
		    <div class="col-sm-4">
		      <input name="nombreusuario" type="text" class="form-control" id="usuario" placeholder="Usuario">
		    </div>
		  </div>

		  <div class="form-group">
		    <label for="contrasenia" class="col-md-2 col-sm-3 control-label">Contraseña</label>
		    <div class="col-sm-4">
		      <input name="contrasenia" type="password" class="form-control" id="contrasenia" placeholder="Contraseña">
		    </div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <div class="checkbox">
		        <label>
		          <input type="checkbox"> Recordarme
		        </label>
		      </div>
		    </div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" name="submit" class="btn btn-default btn btn-primary">Iniciar Sesión</button>
		    </div>
		  </div>
		</form>

	</div><!-- Fin del container -->

<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>