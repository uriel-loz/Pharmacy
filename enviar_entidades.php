<?php
	require_once ('includes/funciones/funciones.php');

	$id = validar('id');
	$id_colonia = validar('id_colonia');
	$tipo = validar('tipo');


	if($tipo == 'municipios') {

		try {
			
			require_once ('includes/funciones/bd_conexion.php');

			$stmt_municipio = $conn->prepare("SELECT DISTINCT municipio 
											FROM `direcciones` 
											WHERE identidad = ?;");				

			$stmt_municipio->bind_param("s", $id);
			$stmt_municipio->execute();
			$stmt_municipio->bind_result($municipio);

			$opciones = '';
			while($stmt_municipio->fetch()) {	

				$opciones .= '<option value="' . $municipio .'">' . $municipio . '</option>';
			}

			echo $opciones;
			$stmt_municipio->close();
			$conn->close();

		} catch (Exception $e) {
			$error = $e ->getMessage();
		}
	}else{

		require_once ('includes/funciones/bd_conexion.php');

		$stmt_colonia = $conn->prepare("SELECT asentamiento 
										FROM `direcciones`
										WHERE municipio = ?;");
		$stmt_colonia->bind_param("s", $id_colonia);
		$stmt_colonia->execute();
		$stmt_colonia->bind_result($colonias);

		$asentamientos = '';
			while($stmt_colonia->fetch()) {	

				$asentamientos .= '<option value="' . $colonias .'">' . $colonias . '</option>';
			}
		echo $asentamientos;

		$stmt_colonia->close();
		$conn->close();
	}

?>

