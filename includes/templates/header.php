<?php 	
	require_once ('includes/funciones/funciones.php');

	session_start();
	confirmar_sesion();

	$usuario = $_SESSION['usuario'];

	try {
		
		require_once ('includes/funciones/bd_conexion.php');

		$rol = "SELECT idusuario_asignar, rol ";
		$rol .= "FROM `roles` ";
		$rol .= "WHERE idusuario_asignar = '{$usuario}'";

		$obtener = $conn->query($rol);


	} catch (Exception $e) {
		
		$error = $e->getMessage();

	}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="css/Normalize.css">
	<link rel="stylesheet" href="css/theme.min.css">
	<link rel="stylesheet" href="css/estilos.css">
	<!-- <link rel="stylesheet" href="../../css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#personal" aria-expanded="false">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="PerfilUsuario.php" class="navbar-brand">Farmacia Lazaro</a>
			</div>
			
			<div class="collapse navbar-collapse" id="personal">

				<?php while($trabajador = $obtener->fetch_assoc() ): ?>
	
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $trabajador['idusuario_asignar']; ?> <span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">

							<li><a href="PerfilUsuario.php"><i class="fa fa-user-circle" aria-hidden="true"></i> Perfil</a></li>
							
							<li><a href="AdministrarMedicamentos.php"><i class="fa fa-heartbeat" aria-hidden="true"></i> Medicamentos</a></li>
							
							<?php if($trabajador['rol'] == 0): ?>

							<li><a href="AdministrarUsuarios.php"><i class="fa fa-id-card" aria-hidden="true"></i> Usuarios</a></li>

							<li><a href="AdministrarPersonal.php"><i class="fa fa-users" aria-hidden="true"></i> Personal</a></li>

							<li><a href="Movimientos.php"><i class="fa fa-list-ol" aria-hidden="true"></i> Movimientos</a></li>

							<li><a href="MovimientosMedicamentos.php"><i class="fa fa-history" aria-hidden="true"></i> Movimientos Medicamentos</a></li>

							<?php endif; ?>

							<li><a href="includes/funciones/cerrar_sesion.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar Sesión</a></li>
						</ul>
					</li>
				</ul>

				<?php endwhile; ?>	

			</div>
		</div>
	</nav> <!-- Barra de navegación Responsiva -->