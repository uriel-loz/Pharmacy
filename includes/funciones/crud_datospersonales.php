<?php ob_flush(); ?>
<?php require_once('funciones.php');?>
<?php 
	if (isset($_POST['submit'])) {

		$usuario = validar('usuario');
		$nombre = validar('nombre');
		$contrasenia = validar('contrasenia');
		$verificar = validar('verificar');
		$apellido_paterno = validar('apellido_paterno');
		$apellido_materno = validar('apellido_materno');
		$rfc = validar('rfc');
		$cel = validar('cel');
		$tel = validar('tel');
		$correo = validar('correo');
		$servidor = validar('servidor');
		$estado = validar('estado');
		$municipio = validar('municipio');
		$colonia = validar('colonia');
		$calle = validar('calle');
		$estudios = validar('estudios');
		$sexo = validar('sexo');
		$iddireccion = $estado.$municipio.$colonia;
		$email = $correo.'@'.$servidor;

		alerta($usuario, "Introduce un usuario");
		alerta($nombre, "Introduce un nombre de usuario");
		alerta($contrasenia, "Introduce un contraseña para el usuario");
		alerta($verificar, "Introduce un contraseña para verificarla");
		alerta($apellido_paterno, "Introduce el apellido paterno");
		alerta($apellido_materno, "Introduce el apellido materno");
		alerta($rfc, "Introduce un rfc");
		alerta($cel, "Introduce un cel");
		alerta($tel, "Introduce un tel");
		alerta($correo, "Introduce un correo");
		alerta($servidor, "Introduce el servidor de correo");
		alerta($estado, "Introduce un estado");
		alerta($municipio, "Introduce un municipio");
		alerta($colonia, "Introduce la colonia");
		alerta($estudios, "Introduce el nivel de estudios");
		alerta($sexo, "Introduce el sexo");
	

		if ($contrasenia == $verificar) {
			
			$password = $contrasenia;
		
			$opciones = array(
				'cost' => 12,
				'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
				);

			$hashed_password = password_hash($password, PASSWORD_BCRYPT, $opciones);
		}else{

				echo "<div class='row'>";
				echo "<div class='alert alert-danger col-md-8 col-md-offset-2' role='alert'>";
				echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'>";
				echo "</span>";
  				echo "<span class='sr-only'>Error:";
  				echo "</span>";
  				echo " Las contraseñas no coinciden";
				echo "</div>";
				echo "</div>";

		}

		try {
			require_once('bd_conexion.php');

			$insertar_datos = "INSERT INTO `datos_personales` ";
			$insertar_datos .= " VALUES ('{$usuario}', '{$nombre}', '{$apellido_paterno}', ";
			$insertar_datos .= "'{$apellido_materno}', '{$rfc}', '{$cel}', '{$tel}', ";
			$insertar_datos .= "'{$iddireccion}', '{$calle}', ";
			$insertar_datos .= "'{$email}', '{$estudios}', '{$sexo}')";

			$datos_personales = $conn->query($insertar_datos);

			$stmt = $conn->prepare("INSERT INTO `usuarios` VALUES (?,?)");
			$stmt->bind_param("ss", $usuario, $hashed_password);
			$stmt->execute();

			$stmt->close();

			$verificar = "SELECT idusuario FROM usuarios ";
			$verificar .= "WHERE idusuario = '{$usuario}'";

			$ejecutar = $conn->query($verificar);

			$row_usuario = $ejecutar->num_rows;

			if ($row_usuario == 1) {
				
				header("Location: index.php");

				echo "<div class='row'>";
				echo "<div class='alert alert-success col-md-8 col-md-offset-2' role='alert'>";
				echo "<span class='glyphicon glyphicon-ok' aria-hidden='true'>";
				echo "</span>";
  				echo "<span class='sr-only'>Registrado:";
  				echo "</span>";
  				echo " Usuario Registrado";
				echo "</div>";
				echo "</div>";

			}else{

				echo "<div class='row'>";
				echo "<div class='alert alert-danger col-md-8 col-md-offset-2' role='alert'>";
				echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'>";
				echo "</span>";
  				echo "<span class='sr-only'>Error:";
  				echo "</span>";
  				echo " Las contraseñas no coinciden";
				echo "</div>";
				echo "</div>";

			}

		} catch (Exception $e) {
			$error = $e->getMessage();
	}
}

	if (isset($_POST['actualizar'])) {

		$name = validar('nombre');
		$last_name = validar('apellido_paterno');
		$mother_last_name = validar('apellido_materno');
		$phone = validar('phone');
		$sex = validar('sexo');
		$mail = validar('email');
		$user = validar('alias');
		//valores recibidos desde el perfil de usuario para actualizar sus datos

		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$idhistorial = $user.$fecha_hora;
		$movimiento = "Cambio los datos del peril";
		//variables para registrar datos en el historial

		try {
			
			require_once ('bd_conexion.php');

			$stmt_actualizar = $conn->prepare("UPDATE `datos_personales`
										SET `nombre` = ?, `apellido_paterno` = ?,
										`apellido_materno` = ?, `tel` = ?, 
										`sexo` = ?, `email` = ?
										WHERE `idusuario` = ?;");

			$stmt_actualizar->bind_param("sssssss", $name, $last_name, $mother_last_name,
											$phone, $sex, $mail, $user);
			$stmt_actualizar->execute();
			$stmt_actualizar->close();

			$desactivar_cuenta = "INSERT INTO `historial`(`idhistorial`, `idusuario`, ";
			$desactivar_cuenta .= "`movimiento`, `fecha_y_hora`) ";
			$desactivar_cuenta .= "VALUES('{$idhistorial}', '{$user}', ";
			$desactivar_cuenta .= "'{$movimiento}', '{$fecha_hora}')";

			$conn->query($desactivar_cuenta);

		} catch (Exception $e) {
			
			$error = $e->getMessage();

		}
	}

	
	if (isset($_POST['password'])) {
		
		$contrasenia_actual = validar('contraseniaactual');
		$nueva_contrasenia = validar('nuevacontrasenia');
		$confirmar_contrasenia = validar('confirmacioncontrasenia');
		$iduser = validar('user');
		//valor que se reciben del perfil de usuario para cambiar la contraseña

		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$idhistorial = $iduser.$fecha_hora;
		$movimiento = "Se ha cambiado la contraseña";		

		try {

			require_once ('bd_conexion.php');

			$contrasenia_guardada = "SELECT contrasenia FROM `usuarios` ";
			$contrasenia_guardada .= "WHERE idusuario = '{$iduser}'";
			
			$consultar = $conn->query($contrasenia_guardada);

			$obtener_contrasenia = $consultar->fetch_array(MYSQLI_ASSOC);

				
			if (password_verify($contrasenia_actual, $obtener_contrasenia['contrasenia'])) {
				
				if ($nueva_contrasenia == $confirmar_contrasenia) {
					
					$newpassword = $nueva_contrasenia;

					$opciones = array(

						'cost' => 12,
						'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)

						); 

					$hashed_password = password_hash($newpassword, PASSWORD_BCRYPT, $opciones);
					

					$stmt_modificar_contrasenia = $conn->prepare("UPDATE `usuarios` 
													SET `contrasenia` = ?
													WHERE `idusuario` = ?;");

					$stmt_modificar_contrasenia->bind_param("ss", $hashed_password, $iduser);
					$stmt_modificar_contrasenia->execute();
					$stmt_modificar_contrasenia->close();

					$desactivar_cuenta = "INSERT INTO `historial`(`idhistorial`, `idusuario`, ";
					$desactivar_cuenta .= "`movimiento`, `fecha_y_hora`) ";
					$desactivar_cuenta .= "VALUES('{$idhistorial}', '{$iduser}', ";
					$desactivar_cuenta .= "'{$movimiento}', '{$fecha_hora}')";		

					echo '<script>alert("Cambio de contraseña realizado");</script>';

				}else{
					echo '<script>alert("Las contrasenias no coinciden");</script>';
				}
			}else{
				echo '<script>alert("Contraseña incorrecta");</script>';
			}
			
		} catch (Exception $e) {
			
			$error = $e->getMessage();

		}
	}
 ?>
<?php ob_end_flush(); ?>