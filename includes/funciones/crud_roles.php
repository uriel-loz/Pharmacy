<?php 
	require_once('funciones.php');
	session_start();
	confirmar_sesion();


	if (isset($_POST['asignar'])) {

		$usuario_administrador = $_SESSION['usuario'];
		$usuario_asignar = validar('usuario');
		$rol = validar('rol');
		$datos_tiempo = validar('datos_tiempo');
		//variables que recibimos de Roles.php

		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$idhistorial = $administrador.$fecha_hora;
		$movimiento = 'Se ha aceptado una solicitud';

		if ($rol >= 0) {
			$estado = 1;
		}

		$idcuenta = $usuario_asignar.$estado;


		try {
			require_once('bd_conexion.php');

			$stmt = $conn->prepare("INSERT INTO `roles` VALUES (?, ?, ?, ?)");
			$stmt->bind_param("ssss", $usuario_administrador, $usuario_asignar, $rol, $datos_tiempo);
			$stmt->execute();

			$stmt->close();

			$stmt_cuentas = $conn->prepare("INSERT INTO `cuentas` VALUES(?, ?, ?)");
			$stmt_cuentas->bind_param("sss", $idcuenta, $usuario_asignar, $estado);
			$stmt_cuentas->execute();
			$stmt_cuentas->close();

			
			$personal = "INSERT INTO `personal` ";
			$personal .= "VALUES('{$usuario_asignar}')";
			$trabajador = $conn->query($personal);

			$aceptar_solicitud = "INSERT INTO `historial`(`idhistorial`, `idusuario`, ";
			$aceptar_solicitud .= "`idcuenta`, `movimiento`, `fecha_y_hora`) ";
			$aceptar_solicitud .= "VALUES('{$idhistorial}', '{$usuario_administrador}', '{$usuario_asignar}', ";
			$aceptar_solicitud .= "'{$movimiento}', '{$fecha_hora}')";

			$registrar = $conn->query($aceptar_solicitud);

			header("location: http://localhost/Farmacia/AdministrarPersonal.php");
			
		} catch (Exception $e) {
			$error = $e->getMessage();
		}

	}

	if (isset($_POST['modificar'])) {

		$administrador = $_SESSION['usuario'];
		$empleado = validar('usuario');
		$rol = $_POST['rol'];
		$datos_tiempo = validar('datos_tiempo');

		//validamos las variables qe viene de editar roles

		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$idhistorial = $administrador.$fecha_hora;
		$movimiento = 'Se ha modificado el rol del usuario';

		try {

			require_once ('bd_conexion.php');

			$reasignar = "UPDATE `roles` ";
			$reasignar .= "SET `idusuario` = '{$administrador}', `rol` = {$rol}, `fecha_y_hora` = '{$datos_tiempo}' ";
			$reasignar .= "WHERE `idusuario_asignar` = '{$empleado}'";

			$modificar = $conn->query($reasignar);

			$modificar_rol = "INSERT INTO `historial`(`idhistorial`, `idusuario`, ";
			$modificar_rol .= "`idcuenta`, `movimiento`, `fecha_y_hora`) ";
			$modificar_rol .= "VALUES('{$idhistorial}', '{$administrador}', '{$empleado}', ";
			$modificar_rol .= "'{$movimiento}', '{$fecha_hora}')";

			$registrar_modificacion = $conn->query($modificar_rol);


			header("location: http://localhost/Farmacia/AdministrarPersonal.php");

		} catch (Exception $e) {

			$error = $e->getMessage();

		}
	}

	if (isset($_GET['idusuario'])) {

		$idsolicitud = validar('idusuario');
		$administrador = $_SESSION['usuario'];
		$borrar_solicitud = 'El usuario elimino los datos de un usuario sin asignar';
		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$idhistorial = $administrador.$fecha_hora;
		
		try {

			require_once ('bd_conexion.php');

			$stmt_usuarios = $conn->prepare("DELETE FROM usuarios WHERE idusuario = ?");
			$stmt_usuarios->bind_param("s", $idsolicitud);
			$stmt_usuarios->execute();
			$stmt_usuarios->close();

			$stmt_datos = $conn->prepare("DELETE FROM datos_personales WHERE idusuario = ?");
			$stmt_datos->bind_param("s", $idsolicitud);
			$stmt_datos->execute();
			$stmt_datos->close();

			$eliminar_solicitud = "INSERT INTO `historial`(`idhistorial`, `idusuario`, ";
			$eliminar_solicitud .= "`idcuenta`, `movimiento`, `fecha_y_hora`) ";
			$eliminar_solicitud .= "VALUES('{$idhistorial}', '{$administrador}', '{$idsolicitud}', ";
			$eliminar_solicitud .= "'{$borrar_solicitud}', '{$fecha_hora}')";

			$eliminar = $conn->query($eliminar_solicitud);

			header("location: http://localhost/Farmacia/AdministrarUsuarios.php");
			
		} catch (Exception $e) {
			
			$error = $e->getMessage();

		}
	}
 ?>