<?php 
	require_once ('funciones.php');

	if (isset($_POST['desactivar'])) {
		
		$usuario = validar('usuario');
		$justificacion = validar('justificacion');
		//informacion de la cuenta ha desactivar

		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$idhistorial = $usuario.$fecha_hora;
		$movimiento = "Se ha desactivado una cuenta";
		//informacion del historial

		try {
			
			require_once('bd_conexion.php');

			$cuentas_activas = "SELECT idcuenta FROM `cuentas` ";
			$cuentas_activas .= "WHERE idusuario = '{$usuario}'";

			$cuenta = $conn->query($cuentas_activas);

			//guardamos el numero de filas que genera la consulata
			$row_cuenta = $cuenta->num_rows;

			$consulta = $cuenta->fetch_array(MYSQLI_ASSOC);

			//inserta el id de la tabla cuentas en la tabla cuentas_inactivas
			$cuenta_inactiva = "INSERT INTO `cuentas_inactivas` ";
			$cuenta_inactiva .= "VALUES ('{$consulta['idcuenta']}', '{$justificacion}')";

			$desactivar = $conn->query($cuenta_inactiva);

			//consultamos los registros que tiene la tabla cuentas_inactivas
			$cuentas_inactivas = "SELECT idcuenta FROM `cuentas_inactivas` ";
			$cuentas_inactivas .= "WHERE idcuenta = '{$consulta['idcuenta']}'";

			$cuenta_desactivada = $conn->query($cuentas_inactivas);

			//guardamos el numero de filas que genera la consulta
			$row_cuenta_desactivada = $cuenta_desactivada->num_rows;

			//comparamos el numero de filas y si es el mismo se pondra un valor cero en la tabla cuentas
			if ($row_cuenta_desactivada == $row_cuenta) {
				
				$estado = '0';
				$idcuenta = $usuario.$estado;

				$actualizar_cuentas = "UPDATE `cuentas` ";
				$actualizar_cuentas .= "SET `idcuenta` = '{$idcuenta}', `idusuario` = '{$usuario}', ";
				$actualizar_cuentas .= "`estado` = '{$estado}' ";
				$actualizar_cuentas .= "WHERE `idcuenta` = '{$consulta['idcuenta']}'";

				$cuentas = $conn->query($actualizar_cuentas);

				$desactivar_cuenta = "INSERT INTO `historial`(`idhistorial`, `idusuario`, ";
				$desactivar_cuenta .= "`idcuenta`, `movimiento`, `fecha_y_hora`, `motivo`) ";
				$desactivar_cuenta .= "VALUES('{$idhistorial}', '{$usuario}', '{$consulta['idcuenta']}', ";
				$desactivar_cuenta .= "'{$movimiento}', '{$fecha_hora}', '{$justificacion}')";

				$conn->query($desactivar_cuenta);

				header("location: http://localhost/Farmacia/AdministrarPersonal.php");
			}


		} catch (Exception $e) {
			
			$error = $e->getMessage();

		}
	}


	if (isset($_GET['idusuario'])) {
		
		$idusuario = validar('idusuario');

		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$idhistorial = $idusuario.$fecha_hora;
		$movimiento = "Se ha reactivado una cuenta";

		try {
			
			require_once ("bd_conexion.php");

			$cuenta = "SELECT idcuenta FROM `cuentas` ";
			$cuenta .= "WHERE idusuario = '{$idusuario}'";

			$idcuenta = $conn->query($cuenta);

			$obtener = $idcuenta->fetch_array(MYSQLI_ASSOC);

			$row_cuenta = $idcuenta->num_rows;

			$cuenta_inactiva = "SELECT idcuenta FROM `cuentas_inactivas` ";
			$cuenta_inactiva .= "WHERE idcuenta = '{$obtener['idcuenta']}'";

			$idcuenta_inactiva = $conn->query($cuenta_inactiva);

			$row_cuenta_inactiva = $idcuenta_inactiva->num_rows;

			if ($row_cuenta_inactiva == $row_cuenta) {

				$estado = '1';
				$idcuenta_activa = $idusuario.$estado;
				$idcuenta_inactiva = $idusuario.'0';
				
				$actualizar_cuentas = "UPDATE `cuentas` ";
				$actualizar_cuentas .= "SET `idcuenta` = '{$idcuenta_activa}', ";
				$actualizar_cuentas .= "`idusuario` = '{$idusuario}', ";
				$actualizar_cuentas .= "`estado` = '{$estado}' ";
				$actualizar_cuentas .= "WHERE `idcuenta` = '{$idcuenta_inactiva}'";

				$actualizar = $conn->query($actualizar_cuentas);

				$borrar_inactiva = "DELETE FROM `cuentas_inactivas` ";
				$borrar_inactiva .= "WHERE idcuenta = '{$idcuenta_inactiva}'";

				$reactivar_cuenta = "INSERT INTO `historial`(`idhistorial`, `idusuario`, ";
				$reactivar_cuenta .= "`idcuenta`, `movimiento`, `fecha_y_hora`) ";
				$reactivar_cuenta .= "VALUES('{$idhistorial}', '{$idusuario}', '{$idcuenta_inactiva}', ";
				$reactivar_cuenta .= "'{$movimiento}', '{$fecha_hora}')";

				$conn->query($reactivar_cuenta);

				header("location: http://localhost/Farmacia/AdministrarPersonal.php");
			}

		} catch (Exception $e) {
			
			$error = $e->getMessage();

		}
	}

 ?>