<?php 
	
	require_once('funciones.php');

	session_start();
	confirmar_sesion();

	$idusuario = $_SESSION['usuario'];

	if($_POST && count($_POST) == 6){ //verifica si se recibe post

	$nombre_medicamento = validar('nombre_medicamento');
	$ingrediente = validar('ingrediente_activo');
	$lote = validar('lote');
	$presentacion = validar('presentacion');
 	$caducidad = validar('caducidad');
	$cantidad = validar('cantidad');
	$idmedicamento = $nombre_medicamento.$ingrediente.$presentacion;
	$idmedicamento_dato = $idmedicamento.$lote.$caducidad;
	//valida las variables de los medicamentos

	$hoy = getdate();
	$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
	$movimiento = "Registrar Medicamento";
// 	//variables del historial

	try {
		require_once('bd_conexion.php');

		$medicamento_existente = "SELECT idmedicamento ";
		$medicamento_existente .= "FROM `medicamentos` ";
		$medicamento_existente .= "WHERE idmedicamento = '{$idmedicamento}'";

		$existente = $conn->query($medicamento_existente);

		$row_medicamento = $existente->num_rows;

		if ($row_medicamento == 0) {
			$nuevo_medicamento = "INSERT INTO `medicamentos` ";
			$nuevo_medicamento .= "VALUES ('{$idmedicamento}', '{$nombre_medicamento}', ";
			$nuevo_medicamento .= "'{$ingrediente}', '{$presentacion}')";

			$conn->query($nuevo_medicamento);
		}

		//realiza la inserción en la base de datos

		$datos_medicamento = "INSERT INTO `datos_medicamentos` ";
		$datos_medicamento .= "VALUES ('{$idmedicamento_dato}', '{$idmedicamento}', ";
		$datos_medicamento .= "'{$lote}', '{$caducidad}', '{$cantidad}')";

		$conn->query($datos_medicamento);

		$historial_medicamento = "INSERT INTO `historial_medicamentos` ";
		$historial_medicamento .= "VALUES('{$idusuario}', '{$idmedicamento_dato}', ";
		$historial_medicamento .= "'{$movimiento}', '{$cantidad}', '{$fecha_hora}')";

		$conn->query($historial_medicamento);
		
	} catch (Exception $e) {
		$error = $e->getMessage();
		//envia un mensaje de error si no se pudo establecer la conexión a la base de datos
	}
	
}
// Actualiza la información de los medicamento
	else if ($_POST && count($_POST) == 9) {

		session_start();
		confirmar_sesion();

		$idusuario = $_SESSION['usuario'];

		$nombre_medicamento = validar('nombre_medicamento');
		$ingrediente = validar('ingrediente_activo');
		$caducidad = validar('caducidad');
		$lote = validar('lote');
		$presentacion = validar('presentacion');
		$operacion = validar('operacion');
		$cantidad = validar('cantidad');
		$id = $_POST['id'];
		$idmedicamento = $nombre_medicamento.$ingrediente.$presentacion;
		$idmedicamento_dato = $idmedicamento.$lote.$caducidad;
	//validamos la informacion para editar el medicamento

		$actualizar_movimiento = 'Se ha cambiado la cantidad de medicamento disponible';
		$justificacion = validar('justificacion');
		$hoy = getdate();
		$fecha_hora = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].":".$hoy['minutes'].":".$hoy['seconds'];
		$cantidad_final = operation($cantidad, $operacion);
	//validamos la justificacion que se ingresara en el historial
	
	try {
		require_once('bd_conexion.php');

		$modificar_medicamento = "UPDATE `medicamentos` ";
		$modificar_medicamento .= "SET `idmedicamento` = '{$idmedicamento}', ";
		$modificar_medicamento .= "`nombre` = '{$nombre_medicamento}', ";
		$modificar_medicamento .= "`ingrediente_activo` = '{$ingrediente}', ";
		$modificar_medicamento .= "`presentacion` = '{$presentacion}' ";
		$modificar_medicamento .= "WHERE `idmedicamento` = '{$id}'";
		
		$conn->query($modificar_medicamento);

		$modificar_dato = "UPDATE `datos_medicamentos` ";
		$modificar_dato .= "SET `iddato_medicamento` = '{$idmedicamento_dato}', ";
		$modificar_dato .= "`idmedicamento` = '{$idmedicamento}', ";
		$modificar_dato .= "`numero_lote` = '{$lote}', ";
		$modificar_dato .= "`fecha_caducidad` = '{$caducidad}', ";
		$modificar_dato .= "`cantidad` = '{$cantidad_final}' ";
		$modificar_dato .= "WHERE `idmedicamento` = '{$id}'";
		
		$conn->query($modificar_dato);
	//actualizamos el contenido de acuerdo a la información de las variables

		$historial_medicamento = "INSERT INTO `historial_medicamentos` ";
		$historial_medicamento .= "VALUES('{$idusuario}', '{$idmedicamento_dato}', ";
		$historial_medicamento .= "'{$actualizar_movimiento}', '{$operacion}', ";
		$historial_medicamento .= "'{$fecha_hora}')";

		$conn->query($historial_medicamento);

	} catch (Exception $e) {
		$error = $e->getMessage();
	}

	//Nos reenvia a la pagina de administración de medicamentos
	header("location: AdministrarMedicamentos.php");
}
 ?>