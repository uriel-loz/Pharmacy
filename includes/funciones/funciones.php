<?php 
	function validar($elemento){
		if(isset($_POST["$elemento"]) || trim($elemento) != ''){

			$dato = $_POST["$elemento"];
			$filtrar_dato = filter_var($dato, FILTER_SANITIZE_STRING);

			if (strlen($filtrar_dato) > 0 && trim($filtrar_dato)) {
				return $filtrar_dato;
			}

		}

		if(isset($_GET["$elemento"]) || trim($elemento) != ''){
			
			$dato = $_GET["$elemento"];
			$filtrar_dato = filter_var($dato, FILTER_SANITIZE_STRING);

			if (strlen($filtrar_dato) > 0 && trim($filtrar_dato)) {
				return $filtrar_dato;
			}
		}
	}

	function confirmar_sesion(){
		if (!verificar_sesion()) {
			
			header("location: http://localhost/Farmacia/index.php");

		}
	}

	function verificar_sesion(){
		return isset($_SESSION['usuario']);
	}

	function cerrar_sesion(){
		if (isset($_SESSION['usuario'])){

			unset($_SESSION['usuario']);

			session_destroy();

			header("Location: http://localhost/Farmacia/index.php");
			
		}
	}

	function alerta($campo, $aviso){
		
		if (strlen($campo) == 0) {

				echo "<div class='row'>";
				echo "<div class='alert alert-danger col-md-8 col-md-offset-2' role='alert'>";
				echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'>";
				echo "</span>";
  				echo "<span class='sr-only'>Error:";
  				echo "</span>";
  				echo " $aviso";
				echo "</div>";
				echo "</div>";
		}
	}

	function operation($cantidad, $operacion)
	{
		if ($operacion < 0) {
			return ($cantidad - abs($operacion));
		}
		else if($operacion > 0){
			return ($cantidad + abs($operacion));
		}
	}
 ?>
