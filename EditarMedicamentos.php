<?php ob_flush(); ?>
<?php require_once('includes/funciones/crudmedicamentos.php'); ?>
<?php include_once('includes/templates/header.php'); ?>
<?php 

	try {
		require_once('includes/funciones/bd_conexion.php');
		$id = validar('id');
		$obtener_medicamento = "SELECT nombre, ingrediente_activo, numero_lote, "; 
		$obtener_medicamento .= "presentacion, cantidad, fecha_caducidad, ";
		$obtener_medicamento .= "d.idmedicamento ";
		$obtener_medicamento .= "FROM `datos_medicamentos` AS d ";
		$obtener_medicamento .= "JOIN `medicamentos` AS m ";
		$obtener_medicamento .= "WHERE d.idmedicamento = '{$id}'";

		$editar = $conn->query($obtener_medicamento);

	} catch (Exception $e) {
		$error=$e->getMessage();
	}
	
 ?>

	<title>Editar Medicamentos</title>
</head>

<body>
	<header>
		<div class="encabezado">
			<h1>Editar Medicamentos</h1>
		</div>
	</header>		
	<div class="container-fluid">
		<form action="EditarMedicamentos.php" method="post" class="form-horizontal">

			<?php while( $mostrar = $editar->fetch_assoc() ): ?>

			<div class="form-group">
				<label for="medicament" class="col-md-2 col-md-offset-2">Nombre del medicamento:</label>
				<div class="col-md-5">
					<input type="text" value="<?php echo $mostrar['nombre'];?>" name="nombre_medicamento" class="form-control" placeholder="Flanax">
				</div>
			</div>

			<div class="form-group">
				<label for="medicament" class="col-md-2 col-md-offset-2">Ingrediente activo:</label>
				<div class="col-md-5">
					<input type="text" value="<?php echo $mostrar['ingrediente_activo'];?>" name="ingrediente_activo" class="form-control" placeholder="Naproxeno">
				</div>
			</div>

			<div class="form-group">
				<label for="llegada" class="col-md-2 col-md-offset-3">Numero de lote:</label>
				<div class="col-md-3">
					<input type="text" value="<?php echo $mostrar['numero_lote'];?>" name="lote" class="form-control" placeholder="EJE125373412">
				</div>
			</div>

			<div class="form-group">
				<label for="llegada" class="col-md-2 col-md-offset-3">Presentación:</label>
				<div class="col-md-3">
					<input type="text" value="<?php echo $mostrar['presentacion'];?>" name="presentacion" class="form-control" placeholder="250ml">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12">
					<label for="cantidad" class="col-md-2 col-md-offset-3">Cantidad Actual:</label>
					<div class="col-md-2">
						<input type="text" value="<?php echo $mostrar['cantidad'];?>" name="cantidad" class="form-control" readonly="readonly" >
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12">
					<label for="cantidad" class="col-md-2 col-md-offset-3">Ingresa la compra o venta:</label>
					<div class="col-md-3">
						<input type="text" name="operacion" class="form-control" placeholder="500 / -500" >
					</div>
				</div>
			</div>	
								
			<div class="form-group">
				<label for="caducidad" class="col-md-2 col-md-offset-2">Fecha de caducidad: </label>
				<div class="col-md-4">
					<input type="text" value="<?php echo $mostrar['fecha_caducidad'];?>" name="caducidad" class="form-control" placeholder="2016-10-02">
				</div>
			</div>

			<div class="form-group">
				<label for="justificacion" class="col-md-1 col-md-offset-3">Justificación:</label>
				<div class="col-md-5">
					<textarea name="justificacion" id="" rows="5" class="form-control"></textarea>
				</div>
			</div>				

			<input type="hidden" name="id" value="<?php echo $mostrar['idmedicamento']; ?>">

			<div class="form-group" id="botonesregistro">
				<div class="col-md-offset-5">
					<button type="submit" class="btn btn-warning btn-lg">Modificar</button>
					<a class="btn btn-danger btn-lg" href="AdministrarMedicamentos.php">Cancelar</a>
				</div>
			</div>

			<?php endwhile; ?>

		</form>
	</div>
	
<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>

<?php ob_end_flush(); ?>