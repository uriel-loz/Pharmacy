<?php ob_start(); ?>

<?php include_once('includes/templates/header.php'); ?>

<?php 
	include_once('includes/funciones/funciones.php');
	include_once('includes/funciones/crud_cuentas.php');

	$usuario = validar('id');

	try {

		include_once('includes/funciones/bd_conexion.php');

		$stmt = $conn->prepare("SELECT nombre, apellido_paterno, apellido_materno, u.idusuario
								FROM usuarios AS u 
								JOIN datos_personales AS d
								ON u.idusuario = d.idusuario
								WHERE u.idusuario = ?;");

		$stmt->bind_param("s", $usuario);
		$stmt->execute();
		$stmt->bind_result($nombre, $apellido_paterno, $apellido_materno, $nombre_usuario);

	} catch (Exception $e) {
		
		$error = $conn->getMessage();
	}
 ?>

	<title>Cuentas Inactivas</title>
</head>

<body>
	<header>
		<div class="encabezado">
			<h1>Cuentas Inactivas</h1>
		</div>
	</header>

	<div class="container-fluid">
		<form action="CuentasInactivas.php" method="post" class="form-horizontal">

			<?php while($stmt->fetch() ): ?>

			<fieldset disabled>
				<div class="form-group">
					<label for="empleado" class="col-md-2 col-md-offset-1">Nombre del Empleado: </label>
					<div class="col-md-7">
						<input type="text" name="nombre_completo" class="form-control" value="<?php echo $nombre." ".$apellido_paterno." ".$apellido_materno; ?>">
					</div>
				</div>
			</fieldset>

			<div class="form-group">
				<label for="movimiento" class="col-md-1 col-md-offset-4">Usuario:</label>
					<div class="col-md-3">
						<input type="text" name="usuario" class="form-control" value="<?php echo $nombre_usuario; ?>">
					</div>
			</div>

			<?php endwhile; ?>
			<?php $stmt->close(); ?>

			<div class="form-group">
				<label for="justificacion" class="col-md-1 col-md-offset-3">Justificación:</label>
				<div class="col-md-5">
					<textarea name="justificacion" id="" rows="5" class="form-control"></textarea>
				</div>
			</div>
					
			<div class="form-group" id="botonesregistro">
				<div class="col-md-offset-5">
					<button class="btn btn-danger btn-lg" type="submit" name="desactivar" >Desactivar</button>
					<a href="AdministrarPersonal.php" class="btn btn-success btn-lg" role="button">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
	
<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php ob_end_flush(); ?>