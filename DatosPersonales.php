<?php require_once('includes/funciones/crud_datospersonales.php'); ?>
<?php 
	try {
		require_once('includes/funciones/bd_conexion.php');
		//Es necesario para que crud_datospersonales identifique los componentes del formulario

		$sql = "SELECT * ";
		$sql .= "FROM `entidades_federativas`";
		$entidad = $conn->query($sql);

	} catch (Exception $e) {
		$error -> $e->getMessage();
	}
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registrarse</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="css/Normalize.css">
	<link rel="stylesheet" href="css/theme.min.css">
	<link rel="stylesheet" href="css/estilos.css">

</head>
<body>
	<div class="container">
		<div class="encabezado">
			<header>
				<h1>Registrarse</h1>
			</header>
		</div>

		<form action="DatosPersonales.php" method="post">
		
				<div class="form-group col-md-4">
					<label for="nombre">Usuario:</label>
					<input name="usuario" placeholder="usuario" type="text" class="form-control">
				</div>

				<div class="form-group col-md-4">
					<label for="nombre">Contraseña:</label>
					<input name="contrasenia" placeholder="contraseña" type="password" class="form-control">
				</div>

				<div class="form-group col-md-4">
					<label for="nombre">Verificar Contraseña:</label>
					<input name="verificar" placeholder="verificar contraseña" type="password" class="form-control">
				</div>

				<div class="form-group col-md-4">
					<label for="nombre">Nombre:</label>
					<input name="nombre" placeholder="Nombre" type="text" class="form-control">
				</div>
				<div class="form-group col-md-4">
					<label for="Apellido Paterno">Apellido Paterno:</label>
					<input name="apellido_paterno" placeholder="Apellido Paterno" type="text" class="form-control">
				</div>
				<div class="form-group col-md-4">
					<label for="Apellido Materno">Apellido Materno:</label>
					<input name="apellido_materno" placeholder="Apellido Materno" type="text" class="form-control">
				</div>
				<div class="form-group col-md-4">
					<label for="R.F.C.">R.F.C.:</label>
					<input name="rfc" placeholder="R.F.C." type="text" class="form-control">
				</div>
				<div class="form-group col-md-4">
					<label for="Cel">Cel:</label>
					<input name="cel" placeholder="cel" type="text" class="form-control">
				</div>
				<div class="form-group col-md-4">
					<label for="Tel">Tel:</label>
					<input name="tel" placeholder="tel" type="text" class="form-control">
				</div>
				
				<div class="form-group form-inline col-md-offset-3 row">
					<label for="correo electronico">Correo Electrico:</label>
					<input name="correo" placeholder="Correo Electronico" type="text" class="form-control">
					<div class="input-group">
						<span class="input-group-addon">@</span>
					 	<input name="servidor" type="text" class="form-control" id="servidoresdecorreo" aria-describedby="servidoresdecorreo" placeholder="servidor.com">
					</div>
				</div>

				<div class="form-group col-md-4 col-md-offset-4 form-horizontal">
					
				
					<label for="estado">Estado:</label>
					
					<select name="estado" id="estado" class="form-control">
						<option value="">--Selecciona el Estado--</option>
						
						<?php while($estado = $entidad->fetch_assoc() ):  ?>
						<option value="<?php echo $estado['identidad']; ?>">
						<?php echo $estado['entidad']; ?>
						</option>
						
						<?php endwhile; ?>

					</select>
					

					<label for="municipio">Municipio o Delegación:</label>
					<select name="municipio" id="municipio" class="form-control">
						<option value="">--Selecciona el Municipio / o Delegación--</option>
					</select>

					<label for="colonia">Colonia o Barrio:</label>
					<select name="colonia" id="colonia" class="form-control">
						<option value="">--Colonia o Barrio--</option>
					</select>

					<label for="calle">Nombre y numero de calle:</label>
					<input name="calle" type="text" class="form-control" placeholder="Calle:">
				</div>

				<div class="form-group col-md-4 col-md-offset-4 form-horizontal">
					<label for="nivel de estudios">Nivel de Estudios:</label>
					<select name="estudios" id="" class="form-control">
						<option value="">--Seleccionar Opción--</option>
						<option value="secundaria">Secundaria</option>
						<option value="preparatoria">Preparatoria</option>
						<option value="universidad">Universidad</option>
					</select>
				</div>

				<div class="form-horizontal col-md-6 col-md-offset-5">
					<?php 
						$sexo = array(
							'm' => 'Masculino',
							'f' => 'Femenino'
							);
						foreach ($sexo as $key => $sex) {
							echo '<label class = "radio-inline">';
							echo "<input type='radio' name='sexo' value=$key> $sex";
							echo '</label>';
						}
					 ?>
				</div>

				<div class="form-horizontal">
					<div class="form-group col-md-12" id="botonesregistro">
						<button class="btn btn-primary btn-lg" name="submit" type="submit" >Enviar</button>
						<a href="index.php" class="btn btn-danger btn-lg" role="button">Cancelar</a>
					</div>
				</div>
			</div>
		</form>
	</div> <!-- Fin del container -->

<?php include_once('includes/templates/footer.php') ?>
		
	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script>
		$('#estado').change(function() { 
			var idEstado = this.value;

			$.get('enviar_entidades.php', {
				id: idEstado,
				tipo: 'municipios'
			}, function(opciones) {
				$('#municipio').html(opciones);
			});
		})
	</script>

	<script>
		$('#municipio').change(function() {
			var idMunicipio = this.value;

			$.get('enviar_entidades.php', {
				id_colonia: idMunicipio,
				tipo: 'colonia'
			}, function(asentamientos) {
				$('#colonia').html(asentamientos);
			});
		})
	</script>


</body>
</html>