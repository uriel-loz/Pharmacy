<?php require_once('includes/templates/header.php'); ?>

<?php 
	try {
		require_once('includes/funciones/bd_conexion.php');

		$rol = "SELECT nombre, apellido_paterno, apellido_materno, idusuario_asignar, rfc, rol, r.idusuario, estado ";
		$rol .= "FROM usuarios AS u ";
		$rol .= "JOIN datos_personales AS d ";
		$rol .=	"ON u.idusuario = d.idusuario ";
		$rol .=	"JOIN roles AS r ";
		$rol .=	"ON u.idusuario = r.idusuario_asignar ";
		$rol .= "JOIN cuentas AS c ";
		$rol .= "ON u.idusuario = c.idusuario";

		$usuarios = $conn->query($rol);



	} catch (Exception $e) {
		
	}
 ?>

<title>Administración Personal</title>
</head>

<body>
	<header>
		<div class="encabezado">
			<h1>Administración del Personal</h1>
		</div>
	</header>

	<div class="container-fluid">
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>Nombre del Empleado</th>
						<th>Usuario</th>
						<th>R.F.C.</th>
						<th>Puesto</th>
						<th>Autorizo</th>
						<th>Estado</th>
						<th colspan="3">Opciones</th>
					</tr>
				</thead>
				<tbody>
					<?php while ( $obtener = $usuarios->fetch_assoc() ):?>
	
					<tr>
						<td>
							<?php 

								echo $obtener['nombre']." ".$obtener['apellido_paterno']." ".$obtener['apellido_materno'];
							?>	
						</td>
						<td><?php echo $obtener['idusuario_asignar']; ?></td>
						<td><?php echo $obtener['rfc']; ?></td>
						<td>
							<?php 
								if ($obtener['rol'] == 0) {
									echo 'Administrador';
								}else{
									echo 'Usuario';
								}
							?>
						</td>
						<td><?php echo $obtener['idusuario'] ?></td>
						<td>
							<?php
								if ($obtener['estado'] == 1) {
									echo "Activo";
								}else{
									echo "Inactivo";
								}
							?>	
						</td>
						<td>
							<a href="Editar_Roles.php?id=<?php echo $obtener['idusuario_asignar']; ?>" class="btn btn-warning btn-md" role="button">Modificar Puesto</a>
						</td>

						<?php if ($obtener['estado'] == 1): ?>

						<td>
							<a href="#" class="btn btn-success btn-md disabled" role="button">Activar Cuenta</a>
						</td>

						<td>
							<a href="CuentasInactivas.php?id=<?php echo $obtener['idusuario_asignar'] ?>" class="btn btn-danger btn-md" role="button">Desactivar Cuenta</a>
						</td>

						<?php endif; ?>

						<?php if($obtener['estado'] == 0): ?>

						<td>
							<a href="includes/funciones/crud_cuentas.php?idusuario=<?php echo $obtener['idusuario_asignar'] ?>" class="btn btn-success btn-md" role="button">Activar Cuenta</a>
						</td>

						<td>
							<a href="#" class="btn btn-danger btn-md disabled" role="button">Desactivar Cuenta</a>
						</td>

						<?php endif; ?>

					</tr>

					<?php endwhile; ?>

				</tbody>
			</table>
		</div>
	</div>

<?php include_once('includes/templates/footer.php') ?>

	<script src="js/jQuery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>